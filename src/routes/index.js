import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import Splash from '../pages/splash';
import home from '../pages/home';

const Stack =createStackNavigator();

const Route = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
            name="Splash"
            component={Splash}
            options={{headerShown: false}}
            />
             <Stack.Screen
            name="home"
            component={home}
            options={{headerShown: false}}
            />
        </Stack.Navigator>
    );
};

export default Route;