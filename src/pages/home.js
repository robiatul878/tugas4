import React, { Component } from 'react'
import { View,Text, ScrollView, Image } from 'react-native' 

const App = () => {
  return (
    <View>
      <Gambar />
      <ScrollView>
      <Saya />
      </ScrollView>
    </View>
  )
}

const Gambar = () => {
  return(
    <View style= {{height:50, backgroundColor:"#FFFFFF", flexDirection: 'row'}}>
      <Image
       style={{"width":100, "height": 40,marginRight: 90, marginLeft: 10, marginTop: 4}}
       source={require('../assets/yt.png')}/>
    
    <Image
       style={{"width":25, "height": 25, marginRight: 30, marginTop: 10}}
       source={require('../assets/lc.png')}/>
    
    <Image
       style={{width:30, height: 30, marginRight: 30, marginTop: 10}}
       source={require('../assets/sc.png')}/>

<Image
       style={{width:30, height: 30, marginRight: 30, marginTop: 10, borderRadius: 30}}
       source={require('../assets/a.jpg')}/>
    
    </View>
  )
}

const Saya = () => {
  return(
    <View>
      <View>
        <Image
          style={{width:350, height: 200,marginRight: 5, marginLeft: 8, marginTop: 4}}
          source={require('../assets/c.jpg')}/>
      </View>
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 40, height: 40, marginRight: 7,marginLeft:9,marginTop: 10, borderRadius: 30}}
          source={require('../assets/b.jpg')}/>
        <Text style={{backgroundColor: '#FFFFFF', marginTop:15}}>SABYAN - AL WABAA' (Official Music Vidio)</Text>
      </View>
      <View>
        <Image
          style={{"width":350, "height": 250,marginRight: 9, marginLeft: 9, marginTop: 4}}
          source={require('../assets/d.jpg')}/>
      </View>
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 40, height: 40, marginRight: 7,marginLeft:9,marginTop: 10, borderRadius: 30}}
          source={require('../assets/e.jpg')}/>
        <Text style={{backgroundColor: '#FFFFFF', marginTop:17}}>LESTI - KULEPAS DENGAN IKHLAS</Text>
      </View>
      <View>
        <Image
            style={{"width":350, "height": 180,marginRight: 9, marginLeft: 9, marginTop: 4}}
            source={require('../assets/f.jpg')}/>
      </View>
      <View style={{flexDirection: 'row'}}>
        <Image
            style={{width: 40, height: 40, marginRight: 9,marginLeft: 9, marginTop: 10, borderRadius: 30}}
            source={require('../assets/g.jpg')}/>
        <Text style={{backgroundColor: '#FFFFFF', marginTop:15}}>Tutorial Java Script</Text>
      </View>
      <View>
        <Image
            style={{"width":350, "height": 200,marginRight: 9, marginLeft: 9, marginTop: 4}}
            source={require('../assets/ii.jpg')}/>
      </View>
    </View>
  )
}



export default App;
