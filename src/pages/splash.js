import React, {useEffect} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';


const Splash =({navigation}) => {
    useEffect(() => {
     setTimeout (() => {
         navigation.replace('home');
     },5000);
});
return (
    <View style={styles.wrapper}>
        <Image source={require('../assets/splashh.png')} style={{height:130,width:100}}/>
    </View>     
    );
};

export default Splash;

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor:'#FFFF',
        alignItems: 'center',
        justifyContent: 'center',
        flex :2,

    },
    logo:{
        margin: 10,
    },
});
